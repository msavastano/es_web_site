#!"C:\Users\michael\Documents\Visual Studio 2013\Projects\es\es\env\Scripts\python.exe"
# EASY-INSTALL-ENTRY-SCRIPT: 'setuptools==12.1','console_scripts','easy_install'
__requires__ = 'setuptools==12.1'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('setuptools==12.1', 'console_scripts', 'easy_install')()
    )
