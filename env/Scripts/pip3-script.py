#!"C:\Users\michael\Documents\Visual Studio 2013\Projects\es\es\env\Scripts\python.exe"
# EASY-INSTALL-ENTRY-SCRIPT: 'pip==6.0.8','console_scripts','pip3'
__requires__ = 'pip==6.0.8'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('pip==6.0.8', 'console_scripts', 'pip3')()
    )
