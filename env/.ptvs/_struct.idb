�}q (X   docqX  Functions to convert between Python values and C structs.
Python bytes objects are used to hold the data representing the C struct
and also as format strings (explained below) to describe the layout of data
in the C struct.

The optional first format char indicates byte order, size and alignment:
  @: native order, size & alignment (default)
  =: native order, std. size & alignment
  <: little-endian, std. size & alignment
  >: big-endian, std. size & alignment
  !: same as >

The remaining chars indicate types of args and must match exactly;
these can be preceded by a decimal repeat count:
  x: pad byte (no data); c:char; b:signed byte; B:unsigned byte;
  ?: _Bool (requires C99; if not available, char is used instead)
  h:short; H:unsigned short; i:int; I:unsigned int;
  l:long; L:unsigned long; f:float; d:double.
Special cases (preceding decimal count indicates length):
  s:string (array of char); p: pascal string (with count byte).
Special cases (only available in native format):
  n:ssize_t; N:size_t;
  P:an integer type that is wide enough to hold a pointer.
Special case (not in native mode unless 'long long' in platform C):
  q:long long; Q:unsigned long long
Whitespace between formats is ignored.

The variable struct.error is an exception raised on errors.
qX   membersq}q(X   unpackq}q(X   valueq}q(hX�   unpack(fmt, buffer) -> (v1, v2, ...)

Return a tuple containing values unpacked according to the format string
fmt.  Requires len(buffer) == calcsize(fmt). See help(struct) for more
on format strings.q	X	   overloadsq
]q(}q(X   argsq}qX   nameqX   fmtqs}qhX   bufferqs�qX   ret_typeq]qX    qh�qahX�   (v1, v2, ...)

Return a tuple containing values unpacked according to the format string
fmt.  Requires len(buffer) == calcsize(fmt). See help(struct) for more
on format strings.qu}q(X   argsq}q(X   typeq]qX   __builtin__qX   strq�q aX   nameq!X   fmtq"u}q#(h]q$h ah!X   stringq%u�q&X   ret_typeq']q(hX   tupleq)�q*au}q+(h}q,(h]q-h ah!X   fmtq.u}q/(h]q0X   arrayq1X   arrayq2�q3ah!X   bufferq4u�q5h']q6h*au}q7(h}q8(h]q9h ah!X   fmtq:u}q;(h]q<hX   bufferq=�q>ah!X   bufferq?u�q@h']qAh*aueuX   kindqBX   functionqCuX
   __loader__qD}qE(h]qFX   _frozen_importlibqGX   BuiltinImporterqH�qIahBX   typerefqJuX   __name__qK}qL(h}qMX   typeqN]qO(X   builtinsqPX   strqQ�qRh eshBX   dataqSuhH}qT(h}qU(X   mroqV]qW(hIX   builtinsqXX   objectqY�qZeX	   is_hiddenq[�hX�   Meta path import for built-in modules.

    All methods are either class or static methods to avoid the need to
    instantiate the class.

    q\X   basesq]]q^hZah}q_(X   __weakref__q`}qa(h}qb(hN]qchZahX2   list of weak references to the object (if defined)qduhBX   propertyqeuX   __dict__qf}qg(h}qhhN]qiX   builtinsqjX   mappingproxyqk�qlashBhSuX   __lt__qm}qn(h}qo(hX   x.__lt__(y) <==> x<yqph
]qq}qr(h}qs(hN]qtX   builtinsquX   objectqv�qwahX   selfqxu}qyhX   yqzs�q{hX   ==> x<yq|uauhBX   methodq}uX   __str__q~}q(h}q�(hX   x.__str__() <==> str(x)q�h
]q�}q�(h}q�(hN]q�hwahhxu�q�hX
   ==> str(x)q�uauhBh}uX   load_moduleq�}q�(h}q�hN]q�X   builtinsq�X   methodq��q�ashBhSuX   __eq__q�}q�(h}q�(hX   x.__eq__(y) <==> x==yq�h
]q�}q�(h}q�(hN]q�hwahhxu}q�hhzs�q�hX   ==> x==yq�uauhBh}uX   __new__q�}q�(h}q�(hX=   T.__new__(S, ...) -> a new object with type S, a subtype of Tq�h
]q�}q�(h}q�hX   Sq�s}q�(X
   arg_formatq�X   *q�hhu�q�h]q�hahX(   a new object with type S, a subtype of Tq�uauhBhCuX
   __module__q�}q�(h}q�hN]q�hRashBhSuX   __dir__q�}q�(h}q�(hX.   __dir__() -> list
default dir() implementationq�h
]q�}q�(h}q�(hN]q�hwahhxu�q�h]q�X   builtinsq�X   listq��q�ahX   default dir() implementationq�uauhBh}uX
   __format__q�}q�(h}q�(hX   default object formatterq�h
NuhBh}uX   __delattr__q�}q�(h}q�(hX%   x.__delattr__('name') <==> del x.nameq�h
]q�}q�(h}q�(hN]q�hwahhxu}q�hX   argq�s�q�hX   ==> del x.nameq�uauhBh}uX   find_moduleq�}q�(h}q�hN]q�h�ashBhSuX   __ge__q�}q�(h}q�(hX   x.__ge__(y) <==> x>=yq�h
]q�}q�(h}q�(hN]q�hwahhxu}q�hhzs�q�hX   ==> x>=yq�uauhBh}uX   __doc__q�}q�(h}q�hN]q�hRashBhSuX   __hash__q�}q�(h}q�(hX   x.__hash__() <==> hash(x)q�h
]q�}q�(h}q�(hN]q�hwahhxu�q�hX   ==> hash(x)q�uauhBh}uX   __subclasshook__q�}q�(h}q�(hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
q�h
NuhBhCuX
   get_sourceq�}q�(h}q�hN]q�h�ashBhSuX   __init__q�}q�(h}q�(hX>   x.__init__(...) initializes x; see help(type(x)) for signatureq�h
]q�}q�(h}q�(hN]q�hwahhxu}q�(h�h�hhu�q�hX.   initializes x; see help(type(x)) for signatureq�uauhBh}uX
   __sizeof__q�}q�(h}q�(hX6   __sizeof__() -> int
size of object in memory, in bytesq�h
]q�}q�(h}r   (hN]r  hwahhxu�r  h]r  X   builtinsr  X   intr  �r  ahX"   size of object in memory, in bytesr  uauhBh}uX   __setattr__r  }r	  (h}r
  (hX0   x.__setattr__('name', value) <==> x.name = valuer  h
]r  }r  (h}r  (hN]r  hwahhxu}r  hh�s}r  hX   valuer  s�r  hX   ==> x.name = valuer  uauhBh}uX   __le__r  }r  (h}r  (hX   x.__le__(y) <==> x<=yr  h
]r  }r  (h}r  (hN]r  hwahhxu}r  hhzs�r  hX   ==> x<=yr  uauhBh}uX   __repr__r   }r!  (h}r"  (hX   x.__repr__() <==> repr(x)r#  h
]r$  }r%  (h}r&  (hN]r'  hwahhxu�r(  hX   ==> repr(x)r)  uauhBh}uX   module_reprr*  }r+  (h}r,  hN]r-  h�ashBhSuX	   __class__r.  }r/  (h]r0  X   builtinsr1  X   typer2  �r3  ahBhJuX   __gt__r4  }r5  (h}r6  (hX   x.__gt__(y) <==> x>yr7  h
]r8  }r9  (h}r:  (hN]r;  hwahhxu}r<  hhzs�r=  hX   ==> x>yr>  uauhBh}uX   __reduce_ex__r?  }r@  (h}rA  (hX   helper for picklerB  h
NuhBh}uX   get_coderC  }rD  (h}rE  hN]rF  h�ashBhSuX
   is_packagerG  }rH  (h}rI  hN]rJ  h�ashBhSuX   __ne__rK  }rL  (h}rM  (hX   x.__ne__(y) <==> x!=yrN  h
]rO  }rP  (h}rQ  (hN]rR  hwahhxu}rS  hhzs�rT  hX   ==> x!=yrU  uauhBh}uX
   __reduce__rV  }rW  (h}rX  (hX   helper for picklerY  h
NuhBh}uuuhBhNuX   calcsizerZ  }r[  (h}r\  (hX`   calcsize(fmt) -> integer

Return size in bytes of the struct described by the format string fmt.r]  h
]r^  (}r_  (h}r`  hX   fmtra  s�rb  h]rc  j  ahXF   Return size in bytes of the struct described by the format string fmt.rd  u}re  (h}rf  (h]rg  h ah!X   fmtrh  u�ri  h']rj  hX   intrk  �rl  aueuhBhCuh�}rm  (h}rn  hN]ro  (hRhX   NoneTyperp  �rq  eshBhSuX   _clearcacherr  }rs  (h}rt  (hX   Clear the internal cache.ru  h
]rv  }rw  (h)h']rx  jq  auauhBhCuX   packry  }rz  (h}r{  (hX�   pack(fmt, v1, v2, ...) -> bytes

Return a bytes object containing the values v1, v2, ... packed according
to the format string fmt.  See help(struct) for more on format strings.r|  h
]r}  (}r~  (h(}r  hX   fmtr�  s}r�  hX   v1r�  s}r�  hX   v2r�  s}r�  (h�h�hhutr�  h]r�  X   builtinsr�  X   bytesr�  �r�  ahX�   Return a bytes object containing the values v1, v2, ... packed according
to the format string fmt.  See help(struct) for more on format strings.r�  u}r�  (h}r�  (h]r�  h ah!X   fmtr�  u}r�  (h]r�  h*aX
   arg_formatr�  h�h!X   valuesr�  u�r�  h']r�  h aueuhBhCuX   Structr�  }r�  (h]r�  X   builtinsr�  X   Structr�  �r�  ahBhJuX   errorr�  }r�  (h}r�  (hV]r�  (X   structr�  X   errorr�  �r�  X   builtinsr�  X	   Exceptionr�  �r�  X   builtinsr�  X   BaseExceptionr�  �r�  hZehhh]]r�  j�  ah}r�  (h}r�  (h}r�  hN]r�  (hZhX   objectr�  �r�  eshBheuh`}r�  (h}r�  (hN]r�  hZahX2   list of weak references to the object (if defined)r�  uhBheuhf}r�  (h}r�  hN]r�  (hlhX	   dictproxyr�  �r�  eshBhSuhm}r�  (h}r�  (hX   x.__lt__(y) <==> x<yr�  h
]r�  }r�  (h}r�  (hN]r�  hwahhxu}r�  hhzs�r�  hX   ==> x<yr�  uauhBh}uh~}r�  (h}r�  (hX   x.__str__() <==> str(x)r�  h
]r�  (}r�  (h}r�  (hN]r�  hwahhxu�r�  hX
   ==> str(x)r�  u}r�  (h}r�  (h]r�  j�  ah!X   selfr�  u�r�  h']r�  h aueuhBh}uj4  }r�  (h}r�  (hX   x.__gt__(y) <==> x>yr�  h
]r�  }r�  (h}r�  (hN]r�  hwahhxu}r�  hhzs�r�  hX   ==> x>yr�  uauhBh}uh�}r�  (h}r�  (hX   x.__eq__(y) <==> x==yr�  h
]r�  }r�  (h}r�  (hN]r�  hwahhxu}r�  hhzs�r�  hX   ==> x==yr�  uauhBh}uh�}r�  (h}r�  (hX=   T.__new__(S, ...) -> a new object with type S, a subtype of Tr�  h
]r�  (}r�  (h}r�  hh�s}r�  (h�h�hhu�r�  h]r�  hahX(   a new object with type S, a subtype of Tr�  u}r�  (h}r�  (h]r�  hX   typer�  �r�  ah!X   clsr�  u}r�  (h]r�  hX   dictr�  �r�  aj�  X   **r�  h!X   kwArgsr�  u}r�  (h]r�  h*aj�  h�h!X   argsr�  u�r�  h']r   j�  au}r  (h}r  (h]r  j�  ah!X   clsr  u}r  (h]r  h*aj�  h�h!X   argsr  u�r  h']r	  j�  aueuhBhCuh�}r
  (h}r  hN]r  (hRh eshBhSuh�}r  (h}r  (hX.   __dir__() -> list
default dir() implementationr  h
]r  }r  (h}r  (hN]r  hwahhxu�r  h]r  h�ahX   default dir() implementationr  uauhBh}uh�}r  (h}r  (hX   default object formatterr  h
]r  }r  (h}r  (h]r  j�  ah!X   selfr  u}r  (h]r   h ah!X
   formatSpecr!  u�r"  h']r#  h auauhBh}uh�}r$  (h}r%  (hX%   x.__delattr__('name') <==> del x.namer&  h
]r'  (}r(  (h}r)  (hN]r*  hwahhxu}r+  hh�s�r,  hX   ==> del x.namer-  u}r.  (h}r/  (h]r0  j�  ah!X   selfr1  u}r2  (h]r3  h ah!X   namer4  u�r5  h']r6  jq  aueuhBh}uX   __context__r7  }r8  (h}r9  (hN]r:  hZahX   exception contextr;  uhBheuX   with_tracebackr<  }r=  (h}r>  (hXQ   Exception.with_traceback(tb) --
    set self.__traceback__ to tb and return self.r?  h
]r@  }rA  (h}rB  (hN]rC  hwahhxu}rD  hX   tbrE  s�rF  hX-   set self.__traceback__ to tb and return self.rG  uauhBh}uh�}rH  (h}rI  (hX   x.__ge__(y) <==> x>=yrJ  h
]rK  }rL  (h}rM  (hN]rN  hwahhxu}rO  hhzs�rP  hX   ==> x>=yrQ  uauhBh}uh�}rR  (h}rS  (hX   x.__hash__() <==> hash(x)rT  h
]rU  (}rV  (h}rW  (hN]rX  hwahhxu�rY  hX   ==> hash(x)rZ  u}r[  (h}r\  (h]r]  j�  ah!X   selfr^  u�r_  h']r`  jl  aueuhBh}uh�}ra  (h}rb  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
rc  h
NuhBhCuX   __suppress_context__rd  }re  (h}rf  hN]rg  hZashBheuh�}rh  (h}ri  (hX>   x.__init__(...) initializes x; see help(type(x)) for signaturerj  h
]rk  (}rl  (h}rm  (hN]rn  hwahhxu}ro  (h�h�hhu�rp  hX.   initializes x; see help(type(x)) for signaturerq  u}rr  (h}rs  (h]rt  X
   exceptionsru  X   BaseExceptionrv  �rw  ah!X   selfrx  u}ry  (h]rz  h*aj�  h�h!X   argsr{  u�r|  h']r}  jq  aueuhBh}uX	   __cause__r~  }r  (h}r�  (hN]r�  hZahX   exception causer�  uhBheuj  }r�  (h}r�  (hX0   x.__setattr__('name', value) <==> x.name = valuer�  h
]r�  (}r�  (h}r�  (hN]r�  hwahhxu}r�  hh�s}r�  hX   valuer�  s�r�  hX   ==> x.name = valuer�  u}r�  (h}r�  (h]r�  j�  ah!X   selfr�  u}r�  (h]r�  h ah!X   namer�  u}r�  (h]r�  j�  ah!X   valuer�  u�r�  h']r�  jq  aueuhBh}uj  }r�  (h}r�  (hX   x.__le__(y) <==> x<=yr�  h
]r�  }r�  (h}r�  (hN]r�  hwahhxu}r�  hhzs�r�  hX   ==> x<=yr�  uauhBh}uj   }r�  (h}r�  (hX   x.__repr__() <==> repr(x)r�  h
]r�  (}r�  (h}r�  (hN]r�  hwahhxu�r�  hX   ==> repr(x)r�  u}r�  (h}r�  (h]r�  jw  ah!jx  u�r�  h']r�  h aueuhBh}uh�}r�  (h}r�  hN]r�  (X   builtinsr�  X   NoneTyper�  �r�  h eshBhSuj?  }r�  (h}r�  (hX   helper for pickler�  h
]r�  }r�  (h}r�  (h]r�  jw  ah!jx  u}r�  (h]r�  jl  ah!X   protocolr�  u�r�  h']r�  j�  auauhBh}uh�}r�  (h}r�  (hX6   __sizeof__() -> int
size of object in memory, in bytesr�  h
]r�  (}r�  (h}r�  (hN]r�  hwahhxu�r�  h]r�  j  ahX"   size of object in memory, in bytesr�  u}r�  (h}r�  (h]r�  j�  ah!X   selfr�  u�r�  h']r�  jl  aueuhBh}uj.  }r�  (h]r�  j3  ahBhJuX   __traceback__r�  }r�  (h}r�  hN]r�  hZashBheujK  }r�  (h}r�  (hX   x.__ne__(y) <==> x!=yr�  h
]r�  }r�  (h}r�  (hN]r�  hwahhxu}r�  hhzs�r�  hX   ==> x!=yr�  uauhBh}ujV  }r�  (h}r�  (hX   helper for pickler�  h
]r�  }r�  (h}r�  (h]r�  jw  ah!jx  u�r�  h']r�  j�  auauhBh}uX   __setstate__r�  }r�  (h}r�  (hX.   __setstate__(self: BaseException, state: dict)r�  h
]r�  }r�  (h}r�  (h]r�  jw  ah!jx  u}r�  (h]r�  j�  ah!X   stater�  u�r�  h']r�  jq  auauhBh}uuuhBhNuX	   pack_intor�  }r�  (h}r�  (hX  pack_into(fmt, buffer, offset, v1, v2, ...)

Pack the values v1, v2, ... according to the format string fmt and write
the packed bytes into the writable buffer buf starting at offset.  Note
that the offset is a required argument.  See help(struct) for more
on format strings.r�  h
]r�  (}r   (h(}r  hX   fmtr  s}r  hX   bufferr  s}r  hX   offsetr  s}r  hX   v1r  s}r	  hX   v2r
  s}r  (h�h�hhutr  hX�   Pack the values v1, v2, ... according to the format string fmt and write
the packed bytes into the writable buffer buf starting at offset.  Note
that the offset is a required argument.  See help(struct) for more
on format strings.r  u}r  (h(}r  (h]r  h ah!X   fmtr  u}r  (h]r  h3ah!X   bufferr  u}r  (h]r  jl  ah!X   offsetr  u}r  (h]r  h*aj�  h�h!X   argsr  utr  h']r  jq  aueuhBhCuX   __package__r  }r  (h}r  hN]r   (hRjq  eshBhSuX   unpack_fromr!  }r"  (h}r#  (hX�   unpack_from(fmt, buffer, offset=0) -> (v1, v2, ...)

Return a tuple containing values unpacked according to the format string
fmt.  Requires len(buffer[offset:]) >= calcsize(fmt).  See help(struct)
for more on format strings.r$  h
]r%  (}r&  (h}r'  hX   fmtr(  s}r)  hX   bufferr*  s}r+  (X   default_valuer,  X   0r-  hX   offsetr.  u�r/  h]r0  hahX�   (v1, v2, ...)

Return a tuple containing values unpacked according to the format string
fmt.  Requires len(buffer[offset:]) >= calcsize(fmt).  See help(struct)
for more on format strings.r1  u}r2  (h}r3  (h]r4  h ah!X   fmtr5  u}r6  (h]r7  h3ah!X   bufferr8  u}r9  (h]r:  jl  aX   default_valuer;  j-  h!X   offsetr<  u�r=  h']r>  h*au}r?  (h}r@  (h]rA  h ah!X   fmtrB  u}rC  (h]rD  h ah!X   bufferrE  u}rF  (h]rG  jl  aj;  j-  h!X   offsetrH  u�rI  h']rJ  h*au}rK  (h}rL  (h]rM  h ah!X   fmtrN  u}rO  (h]rP  h>ah!X   bufferrQ  u}rR  (h]rS  jl  aj;  j-  h!X   offsetrT  u�rU  h']rV  h*aueuhBhCuuu.