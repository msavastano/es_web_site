"""
Definition of views.
"""

from django.shortcuts import render
from django.http import HttpRequest
from django.http import HttpResponse
from django.template import RequestContext
from datetime import datetime
from .models import Pieces

def home(request):
    """Renders the home page."""
    all_pieces = Pieces.objects.all()
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        context_instance = RequestContext(request,
        {
            'title':'Home Page',
            'year':datetime.now().year,
            'name':'Ernie Savastano',
            'all_pieces':all_pieces,
        })
    )

def work(request):
    all_pieces = Pieces.objects.all()
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/work.html',
        context_instance = RequestContext(request,
        {
            'title':'Work Page',
            'year':datetime.now().year,
            'name':'Ernie Savastano',
            'message':'Your work page.',
            'all_pieces':all_pieces,
            #'path':request.META['p_pic_path'],
        })
    )

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        context_instance = RequestContext(request,
        {
            'title':'Contact',
            'message':'Your contact page.',
            'name':'Ernie Savastano',
            'year':datetime.now().year,
        })
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        context_instance = RequestContext(request,
        {
            'title':'About',
            'message':'Your application description page.',
            'year':datetime.now().year,
            'name':'Ernie Savastano',
        })
    )
