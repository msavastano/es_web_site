"""
Definition of models.
"""

from django.db import models
import datetime

class Pieces(models.Model):
    p_name = models.CharField(max_length=200)
    p_pic_path = models.CharField(max_length=255)
    p_date = models.DateTimeField('date completed')
    p_desc = models.CharField(max_length=512)
    #p_retired = models.CharField(max_length=1)

    def __str__(self):
        return self.p_name

class Retired(models.Model):
    p_retired = models.BooleanField()
    p_piecefk = models.ForeignKey(Pieces)

    def __str__(self):
        return str(self.p_retired)


'''
from django.db import models
import datetime
from django.utils import timezone

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    
    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

class Choice(models.Model):
    question = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):              # __unicode__ on Python 2
        return self.choice_text
'''